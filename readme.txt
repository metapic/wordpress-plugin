=== Metapic ===
Contributors: marcusraketnu
Tags: tagging, images, collage
Requires at least: 4.0.0
Tested up to: 4.9
Requires PHP: 5.4
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Use Metapic to tag images, create collages and link products from our service.

== Description ==

Use Metapic to tag images, link content and create image collages through the WordPress editor.
The links created with our editor are connected to our product feed allowing you to make money from the traffic generated from these links.

Major features in Metapic include:

* Automatically checks all post content for links that can be converted to Metapic links (needs to be activated on the options page).
* Our custom editor fully integrated in TinyMCE for tagging images and linking content.
* A collage editor for making composite images with your favorite products.
* A stat summary on your dashboard.
* A more detailed custom statistics view for your account showing you all the traffic generated for your links.

PS: You'll need a [Metapic account](http://metapic.se/) to use it. You can register an account in the plugin.
Metapic is a free service and all you need to do is register in order to use the service.
Metapic reserves the right to contact you for more personal details in order to facilitate payouts for the traffic you generate.
If you have a multi site installation please contact us and we will help you set up a multi site account.

== Installation ==

Upload the Metapic plugin to your blog, Activate it, then enter your Metapic account credentials in Settings -> Metapic.
If you don't already have an account you can register through a link on the settings page.
Once you have linked your account to your blog the new buttons should show up in TinyMCE and you're ready to start tagging and linking!

== Changelog ==

= 1.1 =
* Initial public release.

= 1.1.1 =
* Fix MTPC_DEBUG constant bug and add 4.0 compability.

= 1.1.2 =
* Fix dashboard js link.

= 1.1.3 =
* Restructure user select list.

= 1.1.4 =
* Many global fixes and adjustments.

= 1.1.5 =
* Remove unnecessary AJAX calls to simplify multisite compability.

= 1.1.6 =
* Allow network administrators to force SSL/https on multisites.

= 1.1.7 =
* Fix blog switching bug when synchronizing statistics for a multisite.

= 1.2.0 =
* Updating plugin dependencies and bump minimum PHP version to 5.5

= 1.2.1 - 1.2.4 =
* Minor bugfixes

= 1.2.5 =
* Lazyload by default

= 1.3.0 =
* Remove all third party dependencies

= 1.3.1 =
* Update auto register functionality

= 1.3.2 =
* Fix frontend scripts

= 1.3.3 =
* Fix dashboard widget bug

= 1.3.4 =
* Fix changing user randomly bug

= 1.3.5 =
* Add that the user can chose country on registration.

= 1.3.6 =
* Add translation for Poland and Italy.
